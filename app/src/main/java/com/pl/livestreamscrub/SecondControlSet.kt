package com.pl.livestreamscrub

import com.brightcove.player.mediacontroller.BrightcoveMediaController
import com.brightcove.player.view.BaseVideoView

class SecondControlSet(videoView: BaseVideoView) : BrightcoveMediaController(videoView, R.layout.brightcove_floating_controls)