package com.pl.livestreamscrub

import com.brightcove.player.mediacontroller.BrightcoveMediaController
import com.brightcove.player.view.BaseVideoView

class FirstControlSet(videoView: BaseVideoView) : BrightcoveMediaController(videoView, R.layout.brightcove_fullscreen_controls)