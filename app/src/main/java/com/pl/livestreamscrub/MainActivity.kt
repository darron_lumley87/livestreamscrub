package com.pl.livestreamscrub

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.brightcove.player.edge.Catalog
import com.brightcove.player.edge.VideoListener
import com.brightcove.player.event.EventType
import com.brightcove.player.model.Video
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val firstControls by lazy { FirstControlSet(video_view) }
    private val secondControls by lazy { SecondControlSet(video_view) }

    private var firstAreCurrentControls = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        switch_controls_button.setOnClickListener { switchControls() }
        switchControls()
        val catalog = Catalog(
            video_view.eventEmitter,
            ACCOUNTID,
            POLICY_KEY
        )

//        seekToWithDidSetEvent(catalog)
        seekToWithoutDidSetEvent(catalog)
    }

    private fun seekToWithoutDidSetEvent(catalog: Catalog) {
        catalog.findVideoByID(VIDEO_ID, object : VideoListener() {
            override fun onVideo(video: Video) {
                video_view.add(video)

                video_view.start()
                video_view.seekTo(5000)
            }
        })
    }

    private fun seekToWithDidSetEvent(catalog: Catalog) {
        catalog.findVideoByID(VIDEO_ID, object : VideoListener() {
            override fun onVideo(video: Video) {
                video_view.add(video)

                video_view.eventEmitter.on(EventType.DID_SET_VIDEO) {
                    video_view.seekTo(5000)
                    video_view.start()
                }
            }
        })
    }

    private fun switchControls() {
        video_view.mediaController = null

        val controls = if (firstAreCurrentControls) {
//            firstControls.isShowControllerEnable = false
//            secondControls.isShowControllerEnable = true
            secondControls
        } else {
//            firstControls.isShowControllerEnable = true
//            secondControls.isShowControllerEnable = false
            firstControls
        }

        video_view.setMediaController(controls)

        firstAreCurrentControls = !firstAreCurrentControls
    }


    companion object {
        private const val ACCOUNTID = ""
        private const val POLICY_KEY = ""
        private const val VIDEO_ID = ""
    }
}
